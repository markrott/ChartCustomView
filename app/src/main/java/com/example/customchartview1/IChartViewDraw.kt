package com.example.customchartview1

import android.content.Context
import android.graphics.Canvas

interface IChartViewDraw {

    fun setupItems(items: List<ChartItem>)

    fun onTouchEvent(context: Context, x: Float, y: Float) {}

    fun onDraw(canvas: Canvas, width: Int, height: Int)
}