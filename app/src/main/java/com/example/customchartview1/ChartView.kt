package com.example.customchartview1

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View

class ChartView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : View(context, attrs, defStyleAttr, defStyleRes) {

    init {
        initChartView(attrs)
    }

    private lateinit var chartViewDraw: IChartViewDraw
    private val items = mutableListOf<ChartItem>()

    fun setItems(tmpItems: List<ChartItem>) {
        items.clear()
        items.addAll(tmpItems)
        chartViewDraw.setupItems(tmpItems)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_UP -> {
                chartViewDraw.onTouchEvent(context, event.x, event.y)
            }
        }
        return true
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        if (canvas == null) return
        if (items.isEmpty()) return

        chartViewDraw.onDraw(canvas, width, height)
    }

    private fun initChartView(attrs: AttributeSet?) {
        if (attrs == null) return
        val ta = context.obtainStyledAttributes(attrs, R.styleable.ChartView)
        chartViewDraw = DrawChart(
            ta,
            paddingLeft = paddingLeft,
            paddingTop = paddingTop,
            paddingRight = paddingRight,
            paddingBottom = paddingBottom
        )
        ta.recycle()
    }
}