package com.example.customchartview1

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import com.example.customchartview1.click.ClickChartItem

class DrawChart(
    ta: TypedArray,
    private val paddingLeft: Int,
    private val paddingTop: Int,
    private val paddingRight: Int,
    private val paddingBottom: Int
) : IChartViewDraw {

    private var space: Float = 5F
    private var orientation = VERTICAL_ORIENTATION

    private lateinit var rect: RectF
    private lateinit var items: List<ChartItem>
    private val clickList = mutableListOf<ClickChartItem>()

    private val textPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val chartPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply { color = Color.RED }

    init {
        orientation = ta.getInt(R.styleable.ChartView_chart_orientation, VERTICAL_ORIENTATION)
        space = ta.getFloat(R.styleable.ChartView_item_space, 5f)
        textPaint.textSize = ta.getFloat(R.styleable.ChartView_chart_text_size, 20F)
        textPaint.color = ta.getColor(R.styleable.ChartView_chart_text_color, Color.WHITE)
    }

    override fun setupItems(items: List<ChartItem>) {
        this.items = items
    }

    override fun onTouchEvent(context: Context, x: Float, y: Float) {
        clickList.firstOrNull { it.filterClickPredicate(x, y, it) }?.let {
            context.showToast((it.item.value).toString())
        }
    }

    override fun onDraw(canvas: Canvas, width: Int, height: Int) {

        val viewportWidth = (width - paddingLeft - paddingRight).toFloat()
        val viewportHeight = (height - paddingTop - paddingBottom).toFloat()

        val itemWidth = viewportWidth / items.size
        val itemHeight = viewportHeight / items.size

        items.forEachIndexed { index, item ->
            if(orientation == VERTICAL_ORIENTATION)  calculateVerticalCoordinates(index, height, itemWidth, viewportHeight, item)
            else calculateHorizontalCoordinates(index, itemHeight, viewportWidth, item)
            canvas.drawRect(rect, chartPaint)
            drawTextInsideRectangle(canvas, item.value.toInt().toString())
        }
    }

    private fun calculateHorizontalCoordinates(index: Int, itemHeight: Float, viewportWidth: Float, item: ChartItem) {

        val l = paddingLeft.toFloat()
        val t = paddingTop + (index * itemHeight) + space / 2
        val r = paddingLeft + viewportWidth / 100F * item.value
        val b = paddingTop + (index + 1) * itemHeight - space / 2

        rect = RectF(l, t, r, b)
        clickList.add(ClickChartItem(item, Pair(l, r), Pair(t, b)))
    }

    private fun calculateVerticalCoordinates(index: Int, height: Int, itemWidth: Float, viewportHeight: Float, item: ChartItem) {

        val l = paddingLeft + (index * itemWidth) + (space / 2);
        val t = height - paddingBottom - viewportHeight / 100 * item.value
        val r = paddingLeft + (index + 1) * itemWidth - space / 2
        val b = (height - paddingBottom).toFloat()

        rect = RectF(l, t, r, b)
        clickList.add(ClickChartItem(item, Pair(l, r), Pair(t, b)))
    }

    private fun drawTextInsideRectangle(canvas: Canvas, str: String) {

        val xOffset = textPaint.measureText(str) * 0.5f
        val yOffset = textPaint.fontMetrics.ascent * -0.4f
        val textX = (rect.centerX()) - xOffset
        val textY = (rect.centerY()) + yOffset

        canvas.drawText(str, textX, textY, textPaint)
    }
}