package com.example.customchartview1.click

import com.example.customchartview1.ChartItem

class ClickChartItem(
    val item: ChartItem,
    private val xPair: Pair<Float, Float>,
    private val yPair: Pair<Float, Float>
){
    fun filterClickPredicate(x: Float, y: Float, item: ClickChartItem) =
        (x >= item.xPair.first && x <= item.xPair.second) &&
                (y >= item.yPair.first && y <= item.yPair.second)
}